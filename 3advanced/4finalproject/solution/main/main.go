package main

import "golangdojo.com/golangdojo/bootcamp/3advanced/4finalproject/solution/handlers"

func main() {
	handlers.Serve()
}
